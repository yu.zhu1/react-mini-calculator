import React, {Component} from 'react';
import './miniCalculator.less';

class MiniCalculator extends Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            outcome: 0
        };
        this.getPlusOne = this.getPlusOne.bind(this);
        this.getMinusOne = this.getMinusOne.bind(this);
        this.getMultiplyTwo = this.getMultiplyTwo.bind(this);
        this.getDivideTwo = this.getDivideTwo.bind(this);
    }

    render() {
    return (
      <section>
        <div>计算结果为:
          <span className="result"> {this.state.outcome} </span>
        </div>
        <div className="operations">
          <button className='add' onClick={this.getPlusOne}>加1</button>
          <button className='minus' onClick={this.getMinusOne}>减1</button>
          <button className='multiply' onClick={this.getMultiplyTwo}>乘以2</button>
          <button className='divide' onClick={this.getDivideTwo}>除以2</button>
        </div>
      </section>
    );
  }


  getPlusOne() {
        var newOutCome = this.state.outcome+1;
        this.setState({
            outcome: newOutCome
        })
  }


  getMinusOne() {
        var newOutCome = this.state.outcome-1;
        this.setState({
          outcome: newOutCome
      })
  }

    getMultiplyTwo() {
        var newOutCome = this.state.outcome*2;
        this.setState({
            outcome: newOutCome
        })
    }

    getDivideTwo() {
        var newOutCome = this.state.outcome/2;
        this.setState({
            outcome: newOutCome
        })
    }



}

export default MiniCalculator;

